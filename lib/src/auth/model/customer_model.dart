class CustomerModel {
  String nama;
  String email;
  int id;
  String kode;
  String token;
  String message;
  int statuscode;

  CustomerModel(
      {this.message,
      this.statuscode,
      this.nama,
      this.email,
      this.id,
      this.kode,
      this.token});

  CustomerModel.fromJson(Map<String, dynamic> json) {
    nama = json['nama'];
    email = json['email'];
    id = json['id'];
    kode = json['kode'];
    token = json['token'];
    message = json['message'];
    statuscode = json['statuscode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nama'] = this.nama;
    data['email'] = this.email;
    data['id'] = this.id;
    data['kode'] = this.kode;
    data['token'] = this.token;
    data['statuscode'] = this.statuscode;
    data['message'] = this.message;
    return data;
  }
}
