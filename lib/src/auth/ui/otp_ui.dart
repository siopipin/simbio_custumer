import 'package:customer_simbio/src/auth/bloc/auth_bloc.dart';
import 'package:customer_simbio/src/auth/ui/login_ui.dart';
import 'package:customer_simbio/src/util/curver_bg.dart';
import 'package:customer_simbio/src/util/global_config.dart';
import 'package:customer_simbio/src/util/response_model.dart';
import 'package:customer_simbio/src/util/toas.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:toast/toast.dart';

class OtpUI extends StatefulWidget {
  OtpUI({Key key}) : super(key: key);

  _OtpUIState createState() => _OtpUIState();
}

class _OtpUIState extends State<OtpUI> {
  BuildContext _scaffoldContext;
  TextEditingController cOtp = TextEditingController();
  var _otp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Builder(builder: (BuildContext context) {
      _scaffoldContext = context;
      return Stack(
        children: <Widget>[
          ClipPath(
            clipper: CurveClipper(),
            child: ChildCurveContainer.container(context),
          ),
          ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 100, bottom: 50),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16),
                          boxShadow: [
                            BoxShadow(color: Colors.grey[350], blurRadius: 20.0)
                          ]),
                      child: Container(
                        padding: EdgeInsets.only(top: 20, bottom: 25),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Container(
                                  alignment: Alignment.topCenter,
                                  child: Image.asset('assets/images/logo.png',
                                      width: 120.0)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                "Verifikasi Akun!",
                                style: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 16.0, top: 4.0, right: 16.0),
                              child: Text(
                                "Silahkan Masukkan Kode Aktifasi Anda",
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, top: 2.0, right: 30.0),
                              child: Text(
                                "Contoh: 989398",
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.only(top: 24.0),
                                child: PinCodeTextField(
                                  controller: cOtp,
                                  pinBoxHeight: 40,
                                  pinBoxWidth: 30,
                                  defaultBorderColor: Warna.primary(),
                                  maxLength: 6,
                                  pinTextStyle: TextStyle(fontSize: 18.0),
                                  onTextChanged: (text) {
                                    setState(() {
                                      _otp = text;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  20.0, 8.0, 20.0, 8.0),
                              child: Material(
                                  borderRadius: BorderRadius.circular(20.0),
                                  color: Warna.secondary(),
                                  elevation: 0.0,
                                  child: MaterialButton(
                                    onPressed: () async {
                                      await doSubmitOTP(
                                          context: context, otp: cOtp.text);
                                    },
                                    minWidth: MediaQuery.of(context).size.width,
                                    child: Text(
                                      "Submit",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0),
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      )),
                ),
              )
            ],
          )
        ],
      );
    }));
  }

  doSubmitOTP({@required BuildContext context, @required String otp}) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      child: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
    new Future.delayed(Duration.zero, () async {
      print("ke sini");
      ResponseModel res = await authBLOC.doCekOTP(otp: otp);
      if (res.statuscode == 200) {
        showToast(res.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        Navigator.of(context).pop();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginUI()));
      } else {
        showToast(res.message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        Navigator.of(context).pop();
      }
    });
  }
}
