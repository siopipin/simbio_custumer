import 'package:customer_simbio/src/auth/model/customer_model.dart';
import 'package:customer_simbio/src/auth/provider/auth_provider.dart';
import 'package:customer_simbio/src/util/response_model.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc {
  AuthProvider authProvider = AuthProvider();
  //EVENT
  doCekOTP({@required String otp}) async {
    ResponseModel responseModel = await authProvider.cekOTP(otp: otp);
    return responseModel;
  }

  doLogin({@required String email, @required String password}) async {
    CustomerModel customerModel =
        await authProvider.doLogin(email: email, password: password);
    return customerModel;
  }

  doDecodeToken({@required String token}) async {
    await authProvider.decodeToken(token: token);
  }
  //STREAM
}

final authBLOC = AuthBloc();