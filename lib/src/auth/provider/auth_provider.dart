import 'dart:convert';
import 'package:customer_simbio/src/auth/model/customer_model.dart';
import 'package:customer_simbio/src/util/flutter_storage.dart';
import 'package:customer_simbio/src/util/global_config.dart';
import 'package:customer_simbio/src/util/response_model.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' show Client;
import 'package:jaguar_jwt/jaguar_jwt.dart';

class AuthProvider {
  Client client = Client();

  Future<ResponseModel> cekOTP({@required String otp}) async {
    try {
      final String url = '${Url.api()}/auth/otp';
      final header = {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      };
      final body = json.encode({
        "otp": otp,
      });
      print(body);
      Response res = await client.post(url, headers: header, body: body);
      print(res.statusCode);
      print(res.statusCode);
      final data = json.decode(res.body);
      data['statuscode'] = res.statusCode;
      if (res.statusCode == 200) {
        await flutterSTORAGE.doSaveOTP(otp);
        data['message'] = "Successfully";
        final result = ResponseModel.fromJson(data);
        return result;
      }
      if (res.statusCode == 400) {
        data['message'] = "Cannot get data from server";
        final result = ResponseModel.fromJson(data);
        return result;
      }
      if (res.statusCode == 401) {
        data['message'] = "Not authorized or invalid token";
        final result = ResponseModel.fromJson(data);
        return result;
      } else {
        data['message'] = "Error from server";
        final result = ResponseModel.fromJson(data);
        return result;
      }
    } catch (e) {
      final data = json.decode(e.toString());
      data['message'] = e.toString();
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }

  Future<CustomerModel> doLogin(
      {@required String email, String password}) async {
    final String url = '${Url.api()}/auth/login-costumer';
    final header = {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
    final body = json.encode({
      'email': email,
      'password': password,
    });
    Response res = await client.post(url, headers: header, body: body);
    print(res.statusCode);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Login Successfully";
      final result = CustomerModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = CustomerModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized";
      final result = CustomerModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = CustomerModel.fromJson(data);
      return result;
    }
  }

  Future decodeToken({@required String token}) async {
    print("Sedang dekode token");
    print("TOKEN SAYA $token");
    final String data = token;
    final parts = data.split('.');
    final payload = parts[1];
    final String decoded = B64urlEncRfc7515.decodeUtf8(payload);
    final result = CustomerModel.fromJson(json.decode(decoded));
    await flutterSTORAGE.doSaveToken(token);
    await flutterSTORAGE.doSaveID(result.id.toString());
    await flutterSTORAGE.doSaveEmail(result.email);
    await flutterSTORAGE.doSaveNama(result.nama);
    return result;
  }
}
