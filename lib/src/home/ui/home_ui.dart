import 'package:customer_simbio/src/home/ui/beranda_ui.dart';
import 'package:customer_simbio/src/profile/ui/profile_ui.dart';
import 'package:customer_simbio/src/util/app_bar.dart';
import 'package:customer_simbio/src/util/curver_bg.dart';
import 'package:customer_simbio/src/util/global_config.dart';
import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';

class HomeUI extends StatefulWidget {
  HomeUI({Key key}) : super(key: key);

  _HomeUIState createState() => _HomeUIState();
}

class _HomeUIState extends State<HomeUI> {
  BuildContext scaffoldContext;
  @override
  void initState() {
    super.initState();
  }

  int currentIndex = 0;
  final List<Widget> _bodyChildren = [
    BerandaUI(),
    ProfileUI(),
    ProfileUI(),
    ProfileUI(),
  ];
  void onTabTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarDefauld(title: "Simbio"),
      body: Builder(
        builder: (BuildContext context) {
          scaffoldContext = context;
          return Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: <Widget>[
                ClipPath(
                  clipper: CurveClipper(),
                  child: ChildCurveContainer.container(context),
                ),
                _bodyChildren[currentIndex],
              ],
            ),
          );
        },
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: currentIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text('Beranda'),
            activeColor: Warna.primary(),
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.playlist_add_check),
              title: Text('Order'),
              activeColor: Warna.primary()),
          BottomNavyBarItem(
              icon: Icon(Icons.message),
              title: Text('Chat'),
              activeColor: Warna.primary()),
          BottomNavyBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              activeColor: Warna.primary()),
        ],
      ),
    );
  }
}
