import 'package:customer_simbio/src/home/model.dart/product_model.dart';
import 'package:customer_simbio/src/util/global_config.dart';
import 'package:flutter/material.dart';

class BookRow extends StatelessWidget {
  PageController pageController = new PageController(viewportFraction: 0.85);

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 176.0,
      width: double.infinity,
      child: new PageView(
          controller: pageController,
          children: books.map((Book book) {
            return Padding(
              padding: EdgeInsets.all(10.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: new BorderRadius.circular(10.0),
                  boxShadow: <BoxShadow>[
                    new BoxShadow(
                        color: Colors.black38,
                        blurRadius: 2.0,
                        spreadRadius: 1.0,
                        offset: new Offset(0.0, 2.0)),
                  ],
                ),
                child: new Column(
                  children: [
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  padding: EdgeInsets.only(left: 10, top: 10),
                                  height: 60.0,
                                  width: 50,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10.0),
                                        topLeft: Radius.circular(10.0)),
                                    child: new Image.asset(book.asset,
                                        fit: BoxFit.cover),
                                  ))
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 5,
                            child: Container(
                              padding: EdgeInsets.only(top: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20.0, bottom: 10.0, top: 10.0),
                                    child: new Text(book.title,
                                        style: const TextStyle(fontSize: 18.0),
                                        textAlign: TextAlign.left),
                                  ),
                                  new Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20.0, bottom: 10.0),
                                    child: new Text(book.author),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          margin: const EdgeInsets.only(right: 20.0),
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.circular(20.0)),
                          child: new ClipRRect(
                            borderRadius: new BorderRadius.circular(50.0),
                            child: new MaterialButton(
                              minWidth: 60.0,
                              onPressed: () {},
                              color: Warna.primary(),
                              child: new Text('Order',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.w500)),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          }).toList()),
    );
  }
}
