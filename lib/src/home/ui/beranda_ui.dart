import 'package:customer_simbio/src/home/ui/product_row_ui.dart';
import 'package:customer_simbio/src/util/global_config.dart';
import 'package:flutter/material.dart';

class BerandaUI extends StatefulWidget {
  BerandaUI({Key key}) : super(key: key);

  _BerandaUIState createState() => _BerandaUIState();
}

class _BerandaUIState extends State<BerandaUI> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Positioned(
        top: 10,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: 90,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffeff2f3),
                        offset: Offset(1, 5.0),
                        blurRadius: 3.0,
                      ),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[topMenu()],
                  ),
                ),
                listProduct(context: context)
              ],
            )),
      ),
    );
  }

  Widget topMenu() {
    return Expanded(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 70,
                        child: new Icon(
                          Icons.open_in_browser,
                          size: 50,
                          color: Warna.primary(),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Invoice",
                              style: TextStyle(
                                color: Warna.primary(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
          VerticalDivider(
            color: Colors.grey,
            width: 1,
          ),
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 75,
                        child: new Icon(
                          Icons.card_giftcard,
                          size: 50,
                          color: Warna.primary(),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Order",
                              style: TextStyle(
                                color: Warna.primary(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
          VerticalDivider(
            color: Colors.grey,
            width: 1,
          ),
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 75,
                        child: new Icon(
                          Icons.history,
                          size: 50,
                          color: Warna.primary(),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "History",
                              style: TextStyle(
                                color: Warna.primary(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget listProduct({BuildContext context}) {
    return Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(height: 20,),
          new Container(
            width: MediaQuery.of(context).size.width,
            color: Warna.secondary(),
            padding: const EdgeInsets.only(top: 10, bottom: 10.0, left: 10),
            child: Row(
              children: <Widget>[
                Icon(Icons.card_travel, color: Colors.white,),
                SizedBox(width: 5,),
                Text('Product',
                style: const TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w200))
              ],
            ),
          ),
          SizedBox(height: 10,),
          new Container(
            height: 350.0,
            width: double.infinity,
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(20.0),
                  topRight: const Radius.circular(20.0)),
              color: Colors.white,
            ),
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  new BookRow(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
