class Book {

  String asset;
  String title;
  String author;

  Book(this.asset, this.title, this.author);

}

List<Book> books = [
  Book('assets/images/logo.png', 'Crown and Bridge', 'Zirconia'),
  Book('assets/images/logo.png', 'Veneer', 'Porcelain'),
  Book('assets/images/logo.png', 'Inlay / Onlay / Uplay / Porcelain', 'Oliver'),
  Book('assets/images/logo.png', 'Crown and Bridge', 'LD Press'),
  Book('assets/images/logo.png', 'Panic', 'Oliver'),
  //Book('assets/images/book4.gif', 'The 5th Wave', 'Rick Yancey')
];
