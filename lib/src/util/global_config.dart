import 'package:flutter/material.dart';

class Warna {
  static primary() => Color(0xffFFCD46);
  static secondary() => Color(0xffFFB300);
}

class Url {
  static api() => 'http://api.simbio.id:3200/mobile';
}