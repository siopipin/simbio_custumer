import 'package:customer_simbio/src/util/global_config.dart';
import 'package:flutter/material.dart';

appBarDefauld({String title}) {
  return AppBar(
    title: new Text("$title"),
    backgroundColor: Warna.primary(),
    elevation: 0,
    actions: <Widget>[
      Icon(
        Icons.notifications,
        size: 25,
      ),
      SizedBox(
        width: 10,
      )
    ],
  );
}
