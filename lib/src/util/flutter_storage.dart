import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class FlutterStorage {
  final storage = new FlutterSecureStorage();

  doSaveToken(String token) async {
    await storage.write(key: 'token', value: token);
  }

  doSaveOTP(String otp) async {
    await storage.write(key: 'otp', value: otp);
  }

  doSaveID(String id) async {
    await storage.write(key: 'iduser', value: id);
  }

  doSaveEmail(String email) async {
    await storage.write(key: 'emailuser', value: email);
  }

  doSaveNama(String nama) async {
    await storage.write(key: 'username', value: nama);
  }

  //READ TOKEN
  doCekToken() async {
    String token = await storage.read(key: 'token');
    if (token == null) {
      return false;
    } else {
      return true;
    }
  }

  doCekOTP() async {
    String otp = await storage.read(key: 'otp');
    if (otp == null) {
      return false;
    } else {
      return true;
    }
  }

  showToken() async {
    String token = await storage.read(key: 'token');
    return token;
  }

  showOTP() async {
    String res = await storage.read(key: 'otp');
    return res;
  }

  showID() async {
    String id = await storage.read(key: 'iduser');
    return id;
  }
  showEmail() async {
    String email = await storage.read(key: 'emailuser');
    return email;
  }
  showNama() async {
    String nama = await storage.read(key: 'username');
    return nama;
  }
}

final flutterSTORAGE = FlutterStorage();
