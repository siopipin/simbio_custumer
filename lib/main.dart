import 'package:customer_simbio/src/auth/ui/login_ui.dart';
import 'package:customer_simbio/src/auth/ui/otp_ui.dart';
import 'package:customer_simbio/src/home/ui/home_ui.dart';
import 'package:customer_simbio/src/util/flutter_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool dologin;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Roboto',
        primarySwatch: Colors.blue,
      ),
      home: flutterSTORAGE.doCekOTP() == false ? OtpUI() : (flutterSTORAGE.doCekOTP() == true || flutterSTORAGE.doCekToken() == false) ? LoginUI() : HomeUI()
    );
  }
}

